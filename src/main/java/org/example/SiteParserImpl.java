package org.example;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SiteParserImpl implements SiteParser {

    private static final Logger log = LoggerFactory.getLogger(SiteParserImpl.class);
    private String dirSaveImg;

    public SiteParserImpl(String dirSaveImg) {
        this.dirSaveImg = dirSaveImg;
    }

    @Override
    public void parseSite(String site) {

        try {
            Document document = Jsoup.connect(site).get();
            Elements elements = document.select("img[src~=(?i)\\.(png|jpe?g|gif)]");
            Path path = Files.createDirectories(Paths.get(dirSaveImg + document.title()));

            log.info("Directory " + path.getFileName() + " created.");
            for (Element image : elements) {

                String img = image.absUrl("src");

                URL url = new URL(img);
                URLConnection conn = url.openConnection();

                String imgSrc = image.attr("src");
                String nameFile = imgSrc.substring(imgSrc.lastIndexOf("/") + 1);

                File pathAbsolute = new File(path.toAbsolutePath() + File.separator + nameFile);

                if (pathAbsolute.isFile()) {
                    log.info("Данные сохранены: " + pathAbsolute);
                }

                try (BufferedInputStream bufferedInputStream = new BufferedInputStream(conn.getInputStream());
                     BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                             new FileOutputStream(pathAbsolute))) {


                    int bytes;
                    while ((bytes = bufferedInputStream.read()) != -1) {
                        bufferedOutputStream.write(bytes);
                    }
                    bufferedOutputStream.flush();


                } catch (IOException e) {
                    log.error(e.getMessage(), e);
                }
            }
            log.info("Download completed");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}