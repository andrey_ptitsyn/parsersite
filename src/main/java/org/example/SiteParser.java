package org.example;

public interface SiteParser {

    void parseSite(String site);
}
